#include <QCoreApplication>

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QTextStream>
#include <QDebug>

void debugQuery(QSqlQuery query) {
    qDebug() << "LAST QUERY: " << query.lastQuery();
    qDebug() << "ROWS AFFECTED: " << query.numRowsAffected();
    qCritical() << "LAST ERROR: " << query.lastError().text();
}

//int countPeople(QSqlQuery query) {
//    int counter = 0;
//    query.exec("SELECT * FROM people");
//    
//    while(query.next())
//        counter++;
//    
//    return counter;
//}

int countPeople(QSqlQuery query) {
    query.exec("SELECT COUNT(*) FROM people");
    query.next();
    return query.value(0).toInt();
}

bool insertRow(QSqlQuery query) {
    query.prepare(
        "INSERT INTO people ( "
        "name, age, alive ) "
        "VALUES ( "
        ":name, :age, :alive );"
                );
    query.bindValue(":name", "agilob");
    query.bindValue(":age", 13);
    query.bindValue(":alive", 1);
    
    return query.exec();
}

int deleteRow(QSqlQuery query) {
    query.exec("DELETE FROM people WHERE id = 1;");
    return query.numRowsAffected();
}

int main(int argc, char *argv[]) {
    QCoreApplication a(argc, argv);
    QTextStream cout(stdout);
    
    //create database connection
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "main");
    
    //set location and filename for the database
    db.setDatabaseName("myDatabase.sqlite3");
    QSqlQuery query(db);
    
    if(db.open()) {
        
        cout << "db opened!\n";
        QSqlQuery query(db);
        
        query.exec("CREATE TABLE people (id INTEGER PRIMARY KEY, name VARCHAR, age INTEGER, alive BOOLEAN);");
        
        if(insertRow(query) == false)
            debugQuery(query);
        
        cout << "number of people before deleting: " << countPeople(query) << "\n";
        cout << "rows deleted: " << deleteRow(query) << "\n";
        cout << "number of people after deleting: " << countPeople(query) << "\n";        
        
    } else {
        cout << "problem opening database: " << db.lastError().text();
    }
    
    a.exit();
    return 0;
}
